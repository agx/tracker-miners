#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_LDFLAGS_MAINT_STRIP = -Wl,-Bsymbolic-functions

BUILDDIR=$(CURDIR)/obj-$(DEB_HOST_GNU_TYPE)

FEATURE_FLAGS = \
	-Dgeneric_media_extractor=gstreamer \
	-Dsystemd_user_services=/usr/lib/systemd/user \
	-Ddocs=true \
	-Dfunctional_tests=true \
	-Dminer_rss=false \
	-Dbattery_detection=upower \
	-Dcharset_detection=icu \
	-Dsystemd_user_services=true \
	-Dsystemd_user_services_dir=/usr/lib/systemd/user/

MESON_TEST_ARGS = \
	--no-rebuild \
	--verbose
TEST_SUITES_FILTER = \
	functional
ifeq ($(DEB_HOST_ARCH),$(findstring $(DEB_HOST_ARCH),hppa powerpc ppc64 s390x sparc64))
TEST_SUITES_FILTER += \
	audio
endif

# Disable 'universe' dependencies (libosinfo & libiptcdata) on Ubuntu
ifeq ($(shell dpkg-vendor --query vendor),Ubuntu)
FEATURE_FLAGS += \
	-Diptc=disabled \
	-Diso=disabled
endif

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- \
		-Dauto_features=enabled \
		$(FEATURE_FLAGS)

override_dh_install:
	find debian/tmp -name 01-writeback.py -print -delete
	find debian/tmp -name tracker-tests -print -delete
	dh_install

override_dh_makeshlibs:
	dh_makeshlibs -X/usr/lib/$(DEB_HOST_MULTIARCH)/tracker-miners-3.0/

override_dh_shlibdeps:
	dh_shlibdeps -Ltracker-extract

override_dh_auto_test:
	dbus-run-session -- \
	    dh_auto_test --no-parallel -- $(MESON_TEST_ARGS) \
	    $(addprefix --no-suite ,${TEST_SUITES_FILTER})
	-dbus-run-session -- \
	    dh_auto_test --no-parallel -- $(MESON_TEST_ARGS) \
	    $(addprefix --suite ,${TEST_SUITES_FILTER})
